<!--
Copyright 2016 International Center for Agricultural Research in the Dry Areas(ICARDA)
 
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->
<html>
<body>

<br><br><br><br>
<label style="font-size:25px" for="valBox">Welcome to Icarda Geocoding tool</label>
<br><br><br><br>

<br><br>
<label for="value0Box">Upload file :</label>
<br><br><br><br>

<form action="upload.php" method="post" enctype="multipart/form-data">
    Select file:
    <input type="file" name="fileToUpload" id="fileToUpload">
    <input type="submit" value="Upload Excel file" name="submit">
</form>

</body>
</html>