**Description :**

In order to geo-reference the missing data, we developed an automated geolocation tool using PHP and MySQL.

The tool will use the google maps API to get the coordinates, by sending requests with read data from the input file. The coordinates will be written to an output csv file.

To verify the found coordinates, the tool generates images to output folder, with site position on the country map.

The tool supports direction and distance geocoding, when the column SITE (the address) takes the expression: 
**X km D of Address
X: number of kilometers.
D: Direction (S for south, N for north, E for east, W for west and for combinations ( NE,NW,SE,SW )**

**The input file :**

The input file should be in CSV format and include the columns describing the targeted site:

SITE_CODE	: The Id of the collection site.

CTY		: ISO code of the country of collection.

PROVINCE	: Province of collecting mission.

ADMIN2	: Second level of administrative area below the country.

ADM1		: First level of administrative division in the country.

ADM2		: Second level of administrative division in the country.

SITE		: Site address.

Also make sure you create **uploads/** folder in your working directory.

**Limitation :**

The google maps API has a limitation of 2500 query per day, make sure the number of rows in input file doesn't exceeding it.

**Google maps API :**

Get a google API Key using a gmail address, and activate the google maps API on it, documentation on: https://developers.google.com/maps/documentation/javascript/get-api-key#get-an-api-key

The key should be defind on upload.php file : define("GooogleApiKey", "Your_API_Key");

**The output :**

The tool generates automatically as output a csv file(results.csv) with the columns:

SITE_CODE 	  : the site ID.	

LAT	      	  : found Latitude/blank if google API couldn’t georeference the address. 

LON	    	  : found longitude/blank if google API couldn’t georeference the address.  
		
Site_description  : collection site information  

Valid_Site_data   : Yes : if the site description data contains only UTF8 non special characters. NO if not. 	

Site_image	  : indicates the image of the located site (‘:’ is replaced with ‘-‘ in image name),  indicates NOT_IN_POLYGON if the found coordinates are not within the country borders, or NOT_FOUND if google maps API couldn’t find the address.

In order to verify the coordinates are within the country, we implemented an image generator from world shapefiles -view **data/** folder-, which uses the library ShapeFile.inc.php (http://www.phpclasses.org/package/1741-PHP-Read-vectorial-data-from-geographic-shape-files.html) for reading, the found coordinates are then tested if within the country borders or not.
The sites generated images are saved to automatically created folder in root directory named Image_Results.

**Image Example :**

![geocoding4.png](https://bitbucket.org/repo/XBKzzg/images/4056027292-geocoding4.png)

**Tool running :**

The tool’s usage is very simple:
1- Go to page : yourPath/index.php 

![geocoding1.png](https://bitbucket.org/repo/XBKzzg/images/2253594874-geocoding1.png)

2- Upload your file with the required columns.

3- The  tool prints verbose processing as following :

![geocoding2.png](https://bitbucket.org/repo/XBKzzg/images/3533602469-geocoding2.png)

4- The output file: results.csv and images folder are created in the current directory.

Example :

![geocoding3.png](https://bitbucket.org/repo/XBKzzg/images/2334635758-geocoding3.png)