<?php
/*

Copyright 2016 International Center for Agricultural Research in the Dry Areas(ICARDA)
 
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/

class siteLocation
{
    private $siteCode = "";
    private $siteAddress = "";
    private $siteFilteredAddress = "";
    private $province = "";
    private $admin2;
    private $adm1;
    private $adm2;
    private $directionDegree;
    private $distance_km;
    private $countryIsoCode = "";//CTY in the database
    private $country = "";
    private $foundCoordinates = false;
    private $latitude = "";
    private $longitude = "";
    private $image = "";

//gets and sets :
//Site code
    public function getSiteCode()
    {
        return $this->siteCode;
    }

    public function setSiteCode($sc)
    {
        $sc = str_replace(';', ' ', $sc);
        $sc = str_replace('&', 'and', $sc);
        $sc = str_replace('/', ' ', $sc);
        $sc = stripslashes($sc);
        $sc = str_replace('"', ' ', $sc);
        $sc = str_replace("'", ' ', $sc);
        $this->siteCode = $sc;
    }

//Site address
    public function getSiteAddress()
    {
        return $this->siteAddress;
    }

    public function setSiteAddress($sa)
    {
        $this->siteAddress = $sa;
    }

//Province
    public function getProvince()
    {
        return $this->province;
    }

    public function setProvince($p)
    {
        $this->province = $p;
    }

//Admin2
    public function getAdmin2()
    {
        return $this->admin2;
    }

    public function setAdmin2($a)
    {
        $this->admin2 = $a;
    }

//ADM1
    public function getAdm1()
    {
        return $this->adm1;
    }

    public function setAdm1($a)
    {
        $this->adm1 = $a;
    }

//ADM2
    public function getAdm2()
    {
        return $this->adm2;
    }

    public function setAdm2($a)
    {
        $this->adm2 = $a;
    }

//Country Iso code
    public function getCountryIsoCode()
    {
        return $this->countryIsoCode;
    }

    public function setCountryIsoCode($c)
    {
        $this->countryIsoCode = $c;
    }

//Country name
    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry($c)
    {
        $this->country = $c;
    }

//Distance from site
    public function setDistance_km($d)
    {
        $this->distance_km = $d;
    }

    public function getDistance_km()
    {
        return $this->distance_km;
    }

//Direct degree to site
    public function setDirectionDegree($d)
    {
        $this->directionDegree = $d;
    }

    public function getDirectionDegree()
    {
        return $this->directionDegree;
    }

//latitude
    public function getLatitude()
    {
        return $this->latitude;
    }

    public function setLatitude($l)
    {
        $this->latitude = $l;
    }

//longitude
    public function getLongitude()
    {
        return $this->longitude;
    }

    public function setLongitude($l)
    {
        $this->longitude = $l;
    }

//corresponding image name
    public function getImgname()
    {
        return $this->image;
    }

    public function setImgName()
    {
        $SITE_CODE = $this->getSiteCode();
        $SITE_CODE = str_replace(' ', '', $SITE_CODE);
        $result_target = "Image_Results/".$SITE_CODE.".png";
        $result_target = str_replace(' ', '', $result_target);
        echo "Image result target: ".$result_target."<br>";
        //return $photo_name;
        $this->image = $result_target;
    }


    public function setSiteFilteredAddress($sfa)
    {
        $this->siteFilteredAddress = $sfa;
    }

    public function getSiteFilteredAddress()
    {
        return $this->siteFilteredAddress;
    }

    public function getFoundCoordinates()
    {
        return $this->foundCoordinates;
    }
	
//filter extra data 
    public function filterExtraData($filtered_address)
    {
        //remove extra data
        $filtered_address = str_replace(";", ",", $filtered_address);
        $filtered_address = str_ireplace("to", " ", $filtered_address);
        $filtered_address = str_ireplace("from", " ", $filtered_address);
        $filtered_address = str_ireplace("environs", " ", $filtered_address);
        $filtered_address = str_ireplace("small", " ", $filtered_address);
        $filtered_address = str_ireplace("road", " ", $filtered_address);
        $filtered_address = str_ireplace("village", " ", $filtered_address);
        $filtered_address = str_ireplace("near", " ", $filtered_address);
        $filtered_address = str_ireplace("valley", " ", $filtered_address);
        //echo "address filtered from km and key words '$filtered_address'<br>";
        //This will remove duplicates from address
        $filtered_address = implode(',', array_unique(explode(',', $filtered_address)));
        //return $filtered_address;
        $this->setSiteFilteredAddress($filtered_address);
    }

    public function filterSiteAddress()
    {
        $address = $this->getSiteAddress();
        $address = str_replace(';', ' ', $address);
        $address = str_replace('"', ' ', $address);
        $address = str_replace("'", ' ', $address);
        $this->setSiteAddress($address);
        //return $address;
    }

//removes the direction letter(s) N, W, E, S, NE, NW, SE, SW, and ' of '
    public function filterAddressDirection($filtered_address, $direction)
    {
        $filtered_address = str_replace($direction, ' ', $filtered_address);
        $filtered_address = str_ireplace(' of ', ' ', $filtered_address);
        $filtered_address = trim($filtered_address);
        $value = explode(' ', $filtered_address, 2);
        $filtered_address = $value[0];
        return $filtered_address;
    }

//get country name from 3-iso code with connection to the database
    public function getCountryFromISOCode($connect)
    {

        $ISO = $this->getCountryIsoCode();
        //echo "getcountryfromiso function ISO value : ".$ISO."<br>";
        $country_from_DB = "";
        $query = "SELECT CTY_NAME FROM countries WHERE ORI='$ISO'";
        $result = mysqli_query($connect, $query);

        if (($row = $result->fetch_array()) !== null) {
            $country_from_DB = $row[0];
        }
        $this->setCountry($country_from_DB);
    }

// function to get  the coordinates
    public function get_lat_long($GooogleApiKey) //$Site is site code to send in url
    {
        //get the Country, Site
        $address = $this->getSiteFilteredAddress();
        $region = $this->getCountry();
        echo "region $region<br>";
        $Site = $this->getSiteCode();

        //echo "Calling function get_lat_lon :<br>";
        //delete white spaces from the end of string
        $address = rtrim($address);

        $address = str_ireplace(' of ', ' ', $address);
        //replace directions with spaces
        $address = str_replace(' N ', ' ', $address);
        $address = str_replace(' E ', ' ', $address);
        $address = str_replace(' S ', ' ', $address);
        $address = str_replace(' W ', ' ', $address);
        //replace commas with spaces
        $region = str_replace(",", " ", $region);
        $address = str_replace(",", " ", $address);

        //replace multiple spaces with only one scpace :
        $region = preg_replace('!\s+!', ' ', $region);
        $address = preg_replace('!\s+!', ' ', $address);
        //replace spaces with the + sign
        $region = str_replace(" ", "+", $region);
        $address = str_replace(" ", "+", $address);

        $address = rtrim($address, '+');
        $address = $address.','.$region;

        echo "The Address .".utf8_encode($address)."<br>";
        echo "The country .".utf8_encode($region)."<br>";

        if ($region != "") {
            echo "locating :json?address=".utf8_encode($address)."&sensor=false&region=".utf8_encode($region)."&libraries=geometry&sensor=false<br>"; 

            $json = file_get_contents(
                'https://maps.googleapis.com/maps/api/geocode/json?address='.utf8_encode($address).'&region='.utf8_encode($region).'&sensor=false&key='.$GooogleApiKey
            );

            if (isset($json)) {
                $json = json_decode($json);
                $status = $json->{'status'};
				//Check google maps response status	
                if ($status != "OK") {
                    echo "Failed with statut $status<br>";
                } else {
                    if (isset($json->{'results'}[0])) {
                        $this->foundCoordinates = true;
                        $this->latitude = $json->{'results'}[0]
                            ->{'geometry'}->{'location'}->{'lat'};
                        $this->longitude = $json->{'results'}[0]
                            ->{'geometry'}->{'location'}->{'lng'};
                    }
                }
            }
        }
    }


}

?>