<?php
/*

Copyright 2016 International Center for Agricultural Research in the Dry Areas(ICARDA)
 
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/
header('Content-Encoding: UTF-8');


include 'mysql_connection.php';
include 'find_column.php';
include 'class_siteLocation.php';

define("North_degree", 0);
define("NorthEast_degree", 45);
define("East_degree", 90);
define("SouthEast_degree", 135);
define("South_degree", 180);
define("SouthWest_degree", -135);
define("West_degree", -90);
define("NorthWest_degree", -45);
define("ResultsFile", 'results.csv');
define("IMAGES_FOLDER", "Image_Results/");
define("GooogleApiKey", "Your_API_Key");

$target_dir = "uploads/";

$target_file = $target_dir.basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.<br>";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 5000000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
    exit;
}
// Allow certain file formats
if ($imageFileType != "csv") {
    echo "Sorry, csv files are accepted <br>";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.<br>";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "The file ".basename($_FILES["fileToUpload"]["name"])." has been uploaded. <br>";
    } else {
        echo "Sorry, there was an error uploading your file. <br>";
    }
}

// Find the columns :
$col = find_column("CTY", $target_file);
$col1 = find_column("SITE_CODE", $target_file);
$col2 = find_column("PROVINCE", $target_file);
$col3 = find_column("SITE", $target_file);
$col4 = find_column("ADMIN2", $target_file);
$col5 = find_column("ADM1", $target_file);
$col6 = find_column("ADM2", $target_file);


if ($col == -1 || $col1 == -1 || $col2 == -1 || $col3 == -1 || $col4 == -1 || $col5 == -1 || $col6 == -1) {
    echo "Couldn't find columns<br>";
    exit;
}

//create results file :
$fp = fopen(ResultsFile, 'a');
//set results file header
$result_header = array('SITE_CODE', 'LAT', 'LON', 'Site_description', 'Valid_Site_data', 'Site_image');
//Create images folder
fputcsv($fp, $result_header);
if (!file_exists(IMAGES_FOLDER)) {
    mkdir(IMAGES_FOLDER, 0777, true);
}

//include javascript google maps API
echo '<script type="text/javascript" 
src="http://maps.googleapis.com/maps/api/js?libraries=geometry&sensor=false"> 
</script>';

//escape the file's header	
if (($fread = fopen($target_file, "r")) !== false) {
    $header = fgetcsv($fread, 1000, ",");
}
while (($data = fgetcsv($fread, 1000, ",")) !== false) {
    echo "-----------------------------------------------------------------------------------<br>";
    //Create site location object
    $siteLocation = new siteLocation();

    //reading the SITE CODE
    if ($data[$col1] != "") {
        $siteLocation->setSiteCode(($data[$col1]));
        $SITE_CODE = $siteLocation->getSiteCode();
        echo "Locating the site code :$SITE_CODE<br>";
    } else {
        echo "No Site Code found<br>";
    }

    //reading the site description
    if ($data[$col3] != "") {
        $siteLocation->setSiteAddress($data[$col3].", ");
        $siteLocation->filterSiteAddress();
        echo "The site addresse :".utf8_encode($siteLocation->getSiteAddress())."<br>";
    } else {
        echo "No Site location found<br>";
    }

    //reading ADMIN2
    if ($data[$col4] != "") {
        $siteLocation->setAdmin2($data[$col4].", ");
        echo "in Admin2 column ".utf8_encode($siteLocation->getAdm2())."<br>";
        $siteLocation->setSiteAddress($siteLocation->getSiteAddress()." ".$siteLocation->getAdmin2());
    } else {
        echo "No Admin2 value found<br>";
    }
    //read ADM1
    if ($data[$col5] != "") {
        $siteLocation->setAdm1($data[$col5].", ");
        echo "in ADM1 column :".utf8_encode($siteLocation->getAdm1())."<br>";
        $siteLocation->setSiteAddress($siteLocation->getSiteAddress()." ".$siteLocation->getAdm1());
    } else {
        echo "No ADM1 value found<br>";
    }
    //reading ADM2
    if ($data[$col6] != "") {
        $siteLocation->setAdm2($data[$col6].", ");
        echo "in ADM2 column :".$siteLocation->getAdm2()."<br>";
        $siteLocation->setSiteAddress($siteLocation->getSiteAddress()." ".$siteLocation->getAdm2());
    } else {
        echo "No ADM2 value found <br>";
    }

    //reading province data
    if ($data[$col2] != "PROVINCE" && ($data[$col2] != "")) {
        $siteLocation->setProvince($data[$col2].", ");
        echo "in PROVINCE column : ".utf8_encode($siteLocation->getProvince())."<br>";
        $siteLocation->setSiteAddress($siteLocation->getSiteAddress()." ".$siteLocation->getProvince());
    } else {
        echo "No province value found<br>";
    }
    //read coutry 3-iso code
    if ($data[$col] != "") {
        $siteLocation->setCountryIsoCode($data[$col]);
        $siteLocation->getCountryFromISOCode($connect);
        echo "The country 3-iso code".$siteLocation->getCountryIsoCode(
            )." The corresponding country from database:".$siteLocation->getCountry()."<br>";
    } else {
        echo "Country 3-iso code not found<br>";
        continue;
    }

    //Test if site address has valid data
		$aValid = array('-', ',', ';', ' ',
			'ö','ÿ','à'	,'á','â','ã','ä','å',
			'æ','ç','è','é','ê','ë','ì','í',
			'î','ï','ð'	,'ñ','ò','ó','ô','õ',
			'ö','ø','ù','ú','û','ü','ý','þ','ÿ',
			'À','Á','Â','Ã','Ä','Å','Æ','Ç','È',
			'É','Ê','Ë','Ì','Í','Î','Ï','Ð','Ñ',
			'Ò'	,'Ó','Ô','Õ','Ö','Ø','Ù','Ú','Û','Ü','Ý','ß'   
			);
    if (ctype_alnum(str_replace($aValid, '', utf8_encode($siteLocation->getSiteAddress()))) === false) {
        echo "Site information ".utf8_encode($siteLocation->getSiteAddress())." must include only alpha-numerics<br>";
        //$current = file_get_contents(ResultsFile);

        $current = $siteLocation->getSiteCode().",,,".str_replace(
                ',',
                ';',
                $siteLocation->getSiteAddress()
            ).", NO".", "."NOT FOUND"."\n";

        file_put_contents(ResultsFile, utf8_encode($current), FILE_APPEND);
        continue;
    }

    //Test if distance in kilometers exist
    if (strpos(strtolower($siteLocation->getSiteAddress()), 'km ') !== false) 
	{
        echo "This address contains direction in KM <br>";
        //Test if direction exist :
        if (
            (strpos($siteLocation->getSiteAddress(), ' N ') !== false) || (strpos($siteLocation->getSiteAddress(),' S ') !== false) ||
            (strpos($siteLocation->getSiteAddress(), ' W ') !== false) || (strpos( $siteLocation->getSiteAddress(),' E ') !== false) ||
            (strpos($siteLocation->getSiteAddress(), ' NE ') !== false) || (strpos($siteLocation->getSiteAddress(),' SE ') !== false) ||
            (strpos($siteLocation->getSiteAddress(), ' NE ') !== false) || (strpos($siteLocation->getSiteAddress(),' SW ' ) !== false)
			) 
		{

            $siteLocation->setDistance_km(floatval($siteLocation->getSiteAddress()));
            //If floatval doesnt return, extract with explode
            if ($siteLocation->getDistance_km() == 0) {
                echo "Extract number of kilometers :<br>";
                $arr = explode("km", strtolower($siteLocation->getSiteAddress()), 2);
                $siteLocation->setDistance_km($arr[0]);
                $siteLocation->setDistance_km(preg_replace('/[^0-9.]+/', '', $siteLocation->getDistance_km()));
            }
            echo "Number of kilometers ".$siteLocation->getDistance_km()."<br>";
            //Direction
            echo "The address ".utf8_encode($siteLocation->getSiteAddress())."<br>";
            $siteLocation->setSiteFilteredAddress(str_ireplace('km ', ' ', $siteLocation->getSiteAddress()));
            $siteLocation->setSiteFilteredAddress(str_replace(';', ',', $siteLocation->getSiteFilteredAddress()));
            $siteLocation->setSiteFilteredAddress(
                str_replace($siteLocation->getDistance_km(), '', $siteLocation->getSiteFilteredAddress())
            );

            $siteLocation->filterExtraData($siteLocation->getSiteFilteredAddress());

            echo "Address filtered from extra data:".utf8_encode($siteLocation->getSiteFilteredAddress())."<br>";

            echo "Look for directions :<br>";

            echo "Looking for address:".utf8_encode($siteLocation->getSiteAddress())."<br>";

            switch (true) {
                case strpos($siteLocation->getSiteAddress(), ' N ') !== false: {
                    $direction = ' N ';
                    $filtered_address = $siteLocation->filterAddressDirection(
                        $siteLocation->getSiteFilteredAddress(),
                        $direction
                    );
                    $siteLocation->setSiteFilteredAddress($filtered_address);
                    echo "Address filtered from direction:".utf8_encode($siteLocation->getSiteFilteredAddress())."<br>";
                    $siteLocation->get_lat_long(GooogleApiKey); // Passing the Iso code
                    echo 'Direction is North<br>';

                    $siteLocation->setDirectionDegree(North_degree);
                    break;
                }

                case strpos($siteLocation->getSiteAddress(), ' E ') !== false: {
                    $direction = ' E ';
                    $filtered_address = $siteLocation->filterAddressDirection(
                        $siteLocation->getSiteFilteredAddress(),
                        $direction
                    );
                    $siteLocation->setSiteFilteredAddress($filtered_address);
                    echo "Address filtered from direction:".utf8_encode($siteLocation->getSiteFilteredAddress())."<br>";
                    $siteLocation->get_lat_long(GooogleApiKey);

                    echo 'Direction is East<br>';
                    $siteLocation->setDirectionDegree(East_degree);
                    break;
                }

                case strpos($siteLocation->getSiteAddress(), ' S ') !== false: {
                    $direction = ' S ';
                    $filtered_address = $siteLocation->filterAddressDirection(
                        $siteLocation->getSiteFilteredAddress(),
                        $direction
                    );
                    $siteLocation->setSiteFilteredAddress($filtered_address);
                    echo "Address filtered from direction:".utf8_encode($siteLocation->getSiteFilteredAddress())."<br>";
                    $siteLocation->get_lat_long(GooogleApiKey);

                    echo 'Direction is South<br>';
                    $siteLocation->setDirectionDegree(South_degree);
                    break;
                }
                case strpos($siteLocation->getSiteAddress(), ' W ') !== false: {

                    $direction = ' W ';
                    $filtered_address = $siteLocation->filterAddressDirection(
                        $siteLocation->getSiteFilteredAddress(),
                        $direction
                    );
                    $siteLocation->setSiteFilteredAddress($filtered_address);
                    echo "Address filtered from direction:".utf8_encode($siteLocation->getSiteFilteredAddress())."<br>";
                    $siteLocation->get_lat_long(GooogleApiKey);

                    echo 'Direction is West<br>';
                    $siteLocation->setDirectionDegree(West_degree);
                    break;
                }
                case strpos($siteLocation->getSiteAddress(), ' NE ') !== false: {
                    $direction = ' NE ';
                    $filtered_address = $siteLocation->filterAddressDirection(
                        $siteLocation->getSiteFilteredAddress(),
                        $direction
                    );
                    $siteLocation->setSiteFilteredAddress($filtered_address);
                    echo "Address filtered from direction:".utf8_encode($siteLocation->getSiteFilteredAddress())."<br>";
                    $siteLocation->get_lat_long(GooogleApiKey);

                    echo 'Direction is North east<br>';
                    $siteLocation->setDirectionDegree(NorthEast_degree);
                    break;
                }
                case strpos($siteLocation->getSiteAddress(), ' SE ') !== false: {
                    $direction = ' SE ';
                    $filtered_address = $siteLocation->filterAddressDirection(
                        $siteLocation->getSiteFilteredAddress(),
                        $direction
                    );
                    $siteLocation->setSiteFilteredAddress($filtered_address);
                    echo "Address filtered from direction:".utf8_encode($siteLocation->getSiteFilteredAddress())."<br>";
                    $siteLocation->get_lat_long(GooogleApiKey);

                    echo 'Direction is South East<br>';

                    $siteLocation->setDirectionDegree(SouthEast_degree);
                    break;
                }
                case strpos($siteLocation->getSiteAddress(), ' SW ') !== false: {

                    $direction = ' SW ';
                    $filtered_address = $siteLocation->filterAddressDirection(
                        $siteLocation->getSiteFilteredAddress(),
                        $direction
                    );
                    $siteLocation->setSiteFilteredAddress($filtered_address);
                    echo "Address filtered from direction:".utf8_encode($siteLocation->getSiteFilteredAddress())."<br>";
                    $siteLocation->get_lat_long(GooogleApiKey);
                    echo 'Direction is South West<br>';

                    $siteLocation->setDirectionDegree(SouthWest_degree);
                    break;
                }

                case strpos($siteLocation->getSiteAddress(), ' NW ') !== false: {
                    $direction = ' NW ';
                    $filtered_address = $siteLocation->filterAddressDirection(
                        $siteLocation->getSiteFilteredAddress(),
                        $direction
                    );
                    $siteLocation->setSiteFilteredAddress($filtered_address);
                    echo "Address filtered from direction:".utf8_encode($siteLocation->getSiteFilteredAddress())."<br>";
                    //fetchCoordinates ,edit getFoundCoordinates
                    $siteLocation->get_lat_long(GooogleApiKey);

                    echo 'Direction is North West<br>';
                    $siteLocation->setDirectionDegree(NothWest_degree);
                    break;
                }


            }
			//test if coordinates are found
            if ($siteLocation->getFoundCoordinates() === true) {
                $siteLocation->setImgName();
                echo "Photo name :".$siteLocation->getImgName()."<br>";
                echo "found coordinates latitude ".$siteLocation->getLatitude()." and longitude :".$siteLocation->getLongitude()."<br>";
				//Calculate spherical coordinates in function of the found lat/lon and direction degree:
                echo '<script>
							var point = new google.maps.LatLng("'.$siteLocation->getLatitude().'","'.$siteLocation->getLongitude().'" );
							var spherical = google.maps.geometry.spherical; 
							var distance= "'.$siteLocation->getDistance_km() * 1000 .'";
							var spherical_latitude = spherical.computeOffset(point, distance, "'.$siteLocation->getDirectionDegree().'").lat();
							var spherical_longitude = spherical.computeOffset(point, distance, "'.$siteLocation->getDirectionDegree().'").lng();
							</script>';
				//ajax query to make_image.php in order to generate image and verify if are within country borders
                echo '<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>';
                echo '<script>
						$.ajax({
							type: "POST",
							url: "make_image.php",
							data: {
							"latitude" : spherical_latitude,
							"longitude" : spherical_longitude,
							"CTY" : "'.$siteLocation->getCountryIsoCode().'",
							"SITE_CODE" : "'.$siteLocation->getSiteCode().'",
							"photo_name" : "'.$siteLocation->getImgName().'",
							"address" : "'.$siteLocation->getSiteAddress().'",
							"ResultsFile" : "'.ResultsFile.'",
							"IMAGES_FOLDER" :"'.IMAGES_FOLDER.'"
						  },
						  async:false,
						  complete: function(data){
								console.log("Successful");
								}
						});
						
						</script>';
            } else {
                //write not found
                $siteLocation->setSiteAddress(rtrim($siteLocation->getSiteAddress(), ','));
                $siteLocation->setSiteAddress(str_replace(',', ';', $siteLocation->getSiteAddress()));

                $current = $siteLocation->getSiteCode().",,,".$siteLocation->getSiteAddress().", YES".", "."NOT FOUND"."\n";
                file_put_contents(ResultsFile, utf8_encode($current), FILE_APPEND);

            }
        }

    } else {
        echo "Adresse doesnt contain distance<br>";
        echo "The adress is ".utf8_encode($siteLocation->getSiteAddress())."<br>";
        //delete extra data
        $siteLocation->filterExtraData($siteLocation->getSiteAddress());
        $siteLocation->getSiteFilteredAddress();
        echo "Looking for address :".utf8_encode($siteLocation->getSiteAddress())."<br>";

        //fetching coordinates
        $siteLocation->get_lat_long(GooogleApiKey);
		//test if coordinates are found
        if ($siteLocation->getFoundCoordinates() === true) {

            $siteLocation->setImgName();

            echo "Photo name :".$siteLocation->getImgName().", the address is ".utf8_encode($siteLocation->getSiteAddress())."<br>";
            $siteLocation->setSiteAddress(str_replace(',', ';', $siteLocation->getSiteAddress()));

            echo "found coordinates latitude ".$siteLocation->getLatitude()." and longitude :".$siteLocation->getLongitude()."<br>";
			//ajax query to make_image.php in order to generate image and verify if are within country borders
            echo '<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>';
            echo '<script>
						$.ajax({
						type: "POST",
						url: "make_image.php",
						data: {
						"latitude" : "'.$siteLocation->getLatitude().'",
						"longitude" : "'.$siteLocation->getLongitude().'",
						"CTY" : "'.$siteLocation->getCountryIsoCode().'",
						"SITE_CODE" : "'.$siteLocation->getSiteCode().'",
						"photo_name" : "'.$siteLocation->getImgname().'",
						"address" : "'.$siteLocation->getSiteAddress().'",
						"ResultsFile" : "'.ResultsFile.'",
						"IMAGES_FOLDER" :"'.IMAGES_FOLDER.'"
						},
						async:false,
						complete: function(data){
							console.log("Successful");
							}
						});
						
						</script>';
        } else {
            echo "Coordinates not found<br>";
            //write not found
            $siteLocation->setSiteAddress(rtrim($siteLocation->getSiteAddress(), ','));
            $siteLocation->setSiteAddress(str_replace(',', ';', $siteLocation->getSiteAddress()));
            //$current = file_get_contents(ResultsFile);

            $current = $siteLocation->getSiteCode().",,,".$siteLocation->getSiteAddress().", YES".", "."NOT FOUND"."\n";
            file_put_contents(ResultsFile, utf8_encode($current), FILE_APPEND);

        }

    }
    unset($siteLocation);
}


// close the file connection
fclose($fp);
fclose($fread);

chown($target_file, 666);
unlink($target_file);
?>