<?php
/*

Copyright 2016 International Center for Agricultural Research in the Dry Areas(ICARDA)
 
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/

//Requires to use shapefile
require './libs/ShapeFile.inc.php';
$options = array('noparts' => false);
$shp = new ShapeFile("./data/5961.shp", $options); // along this file the class will use file.shx and file.dbf


function getlocationcoords($lat, $lon, $width, $height)
{
    $x = (($lon + 180) * ($width / 360));
    $y = ((($lat * -1) + 90) * ($height / 180));

    return array("x" => round($x), "y" => round($y));
}

function is_in_polygon($points_polygon, $vertices_x, $vertices_y, $my_long, $my_lat)
{
    $i = $j = $c = 0;
    for ($i = 0, $j = $points_polygon; $i < $points_polygon; $j = $i++) {
        if ((($vertices_y[$i] > $my_lat != ($vertices_y[$j] > $my_lat)) &&
            ($my_long < ($vertices_x[$j] - $vertices_x[$i]) * ($my_lat - $vertices_y[$i]) / ($vertices_y[$j] - $vertices_y[$i]) + $vertices_x[$i]))
        ) {
            $c = !$c;
        }
    }

    return $c;
}

$lat = $_POST['latitude'];
$long = $_POST['longitude'];
$CTY = $_POST['CTY'];
$SITE_CODE = $_POST['SITE_CODE'];
$address = $_POST['address'];
$photo_name = $_POST['photo_name'];
$ResultsFile = $_POST['ResultsFile'];
$IMAGES_FOLDER = $_POST['IMAGES_FOLDER'];


echo "make image country_code $CTY<br>";

if (($lat != "") && ($long != "")) {
    $photo_name = str_replace(':', '-', $photo_name);
    $address = str_replace(',', ';', $address);

    echo "make image photo :$photo_name<br>";

    //defining image size:
    $image_sx = 800;
    $image_sy = 800;
    //defining image scale :
    if (empty($scale)) {
        $scale = 5000;
    }

    $sx = 2 * $scale;
    $sy = $scale;

    //The coordinates will be in the center of the image
    $center = getlocationcoords($lat, $long, $sx, $sy);

    $min_x = $center["x"] - ($image_sx / 2);
    $min_y = $center["y"] - ($image_sy / 2);

    $im = imagecreate($image_sx, $image_sy);

    //colors for land and sea
    $land = imagecolorallocate($im, 0xF7, 0xEF, 0xDE);
    $sea = imagecolorallocate($im, 0xB5, 0xC7, 0xD6);
    //res color to indicate site location
    $red = imagecolorallocate($im, 0xff, 0x00, 0x00);

    //fill the image with sea
    imagefilledrectangle($im, 0, 0, $image_sx, $image_sy, $sea);

    // initialization, array variables for country coordinates
    $vertices_x = array();
    $vertices_y = array();

    while ($record = $shp->getNext()) {
        // read the data
        $shp_data = $record->getShpData();
        $dbf_data = $record->getDbfData();
        //get the country code
        $country_code = $dbf_data['ISO_3_CODE'];

        $country_code = str_replace(' ', '', $country_code);
        //look for country in shp file
        if ($country_code == $CTY) {
            foreach ($shp_data['parts'] as $part) {
                $converted_points = array();
                $coords = array();
                $number_points = 0;

                foreach ($part['points'] as $point) {
                    //get country coordinates, float with 6 digits
                    $coords[] = round($point['x'], 6).','.round($point['y'], 6);
                    $number_points++;
                    $pt = getlocationcoords(($point['y']), ($point['x']), $sx, $sy);
                    $converted_points[] = $pt["x"] - $min_x;
                    $converted_points[] = $pt["y"] - $min_y;
                    //get the x coordinates of the country
                    array_push($vertices_x, round($point['x'], 6));
                    //get the y coordinates of the country
                    array_push($vertices_y, round($point['y'], 6));
                }
                $points_polygon = count($vertices_x) - 1;
            }
            //file the image with polygon
            imagefilledpolygon($im, $converted_points, $number_points, $land);
            //echo "Number of points :".$number_points."<br>";
            $pt["x"] = $center["x"] - $min_x;
            $pt["y"] = $center["y"] - $min_y;

            //test if latitude and longitude are within the polygon :
            if (is_in_polygon($points_polygon, $vertices_x, $vertices_y, $long, $lat)) {
                $current = $SITE_CODE.", ".$lat.", ".$long.", ".$address.", YES".", ".$photo_name."\n";
                file_put_contents($ResultsFile, utf8_encode($current), FILE_APPEND);

                echo "<br>Coordinates found within polygon<br>";
                imagefilledrectangle($im, $pt["x"] - 2, $pt["y"] - 2, $pt["x"] + 2, $pt["y"] + 2, $red);
                $SITE_CODE = str_replace(':', '-', $SITE_CODE);
                $SITE_CODE = str_replace(' ', '', $SITE_CODE);
                $result_target = $IMAGES_FOLDER.$SITE_CODE.".png";
                $result_target = str_replace(' ', '', $result_target);
                echo "image result target :$result_target<br>";
                //create image in target
                imagepng($im, $result_target);
                imagedestroy($im);

            } else {
                //$current = file_get_contents($ResultsFile);
                $current = $SITE_CODE.", ".$lat.", ".$long.", ".utf8_decode($address).", YES".", "."NOT_IN_POLYGON"."\n";
                file_put_contents($ResultsFile, utf8_encode($current), FILE_APPEND);
                echo "the site :$SITE_CODE is out of polygon";
                echo "<br>Coordinates not in polygon";
            }

            break;
        }
    }

} else {
    echo "Coordinates not posted to make_image <br>";
}

?>